import argparse

import dateutil.parser as date_parser
import feedparser
import pandas as pd
import pandas_gbq
from google.oauth2 import service_account


def get_topic_based_news_df(topic_settings):
    """
    Parse RSS feed from specified news sites with given topics and return them as a pandas DataFrame.

    :param topic_settings: dictionary - contains the topics and the link to RSS feed for news sites
    :return: pandas.DataFrame(["title", "summary", "link", "published_dt", "topic", "news_site", "h"])
    """
    news_list = []
    for news_site in topic_settings.keys():

        rss_feed = topic_settings[news_site]["rss_feed"]

        for topic in topic_settings[news_site]["topics"]:
            feed = feedparser.parse(rss_feed.format(topic))

            # title, summary, link, published_date, topic, news_site, h (hash of title)
            news_list_for_single_topic = [
                [inn["title"], inn["summary"], inn["id"], date_parser.parse(inn["published"]), topic, news_site] for inn
                in feed["entries"]]
            news_list = news_list + news_list_for_single_topic

    news_df = pd.DataFrame(news_list, columns=["title", "summary", "link", "published_dt", "topic", "news_site"])

    return news_df


def keep_rows_not_in_bigquery_table(new_df, table_name, project_id):
    """
    Keep new rows of a DataFrame that are not in BigQuery table based on SHA1 of title.

    :param new_df: pandas.DataFrame - news created from rss feed
    :param table_name: str- name of BigQuery table
    :param project_id: str - name of GCP project
    :return: pandas.DataFrame(["title", "summary", "link", "published_dt", "topic", "news_site", "h"]) that are not in
    the given BigQuery table
    """
    existing_df = pandas_gbq.read_gbq('SELECT * FROM {}'.format(table_name), project_id, dialect="standard")
    new_only = new_df[~new_df.isin(existing_df)].dropna()
    print(new_only.head())
    print(new_only.columns)
    return new_only


def create_bq_table(table_name, project_id):
    """
    Create table in BigQuery.

    :param table_name: str- name of BigQuery table
    :param project_id: str - name of GCP project
    """
    pandas_gbq.to_gbq(pd.DataFrame([["", "", "", "", "", ""]],
                                   columns=["title", "summary", "link", "published_dt", "topic", "news_site"]),
                      table_name, project_id, if_exists="fail")


def upload_df_to_bq_table(df, table_name, project_id):
    """
    Upload data to BigQuery table.
    :param df: pd.DataFrame - data to upload
    :param table_name: str- name of BigQuery table
    :param project_id: str - name of GCP project
    """
    pandas_gbq.to_gbq(df, table_name, project_id, if_exists="append", table_schema=[{'name': 'title', 'type': 'STRING'},
                                                                                    {'name': 'summary',
                                                                                     'type': 'STRING'},
                                                                                    {'name': 'link', 'type': 'STRING'},
                                                                                    {'name': 'published_dt',
                                                                                     'type': 'STRING'},
                                                                                    {'name': 'topic', 'type': 'STRING'},
                                                                                    {'name': 'news_site',
                                                                                     'type': 'STRING'}])


def main(project_id, bq_table_name, gc_path):
    """
    Main method.
    :param table_name: str- name of BigQuery table
    :param project_id: str - name of GCP project
    :param gc_path: str - path to credentials file
    """
    credentials = service_account.Credentials.from_service_account_file(
        gc_path
    )

    # Update the in-memory credentials cache (added in pandas-gbq 0.7.0).
    pandas_gbq.context.credentials = credentials

    topic_settings = {"origo": {"topics": ["itthon", "nagyvilag", "gazdasag"],
                                "rss_feed": "http://www.origo.hu/contentpartner/rss/{}/origo.xml"},
                      "index": {"topics": ["belfold", "kulfold", "gazdasag"], "rss_feed": "https://index.hu/{}/rss/"}}
    try:
        create_bq_table(bq_table_name, project_id)
    except pandas_gbq.gbq.TableCreationError:
        print("Table {} already exists.".format(bq_table_name))
    news_df = keep_rows_not_in_bigquery_table(get_topic_based_news_df(topic_settings), bq_table_name, project_id)
    new_count = len(news_df.index.values.tolist())
    if new_count > 0:
        print("Uploading {} new rows".format(new_count))
        upload_df_to_bq_table(news_df, bq_table_name, project_id)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Append news to a table in Google BigQuery.")
    parser.add_argument('--project_id',
                        help='REQUIRED - Google Cloud project id.', required=True)
    parser.add_argument('--bq_table_name',
                        help='REQUIRED - BigQuery table name.', required=True)
    parser.add_argument('--gc_path',
                        help='REQUIRED - Path to JSON credentials for Google Cloud Platform.', required=True)
    args = parser.parse_args()
    main(args.project_id, args.bq_table_name, args.gc_path)
