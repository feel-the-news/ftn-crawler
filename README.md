# ftn-crawler [![pipeline status](https://gitlab.com/feel-the-news/ftn-crawler/badges/master/pipeline.svg)](https://gitlab.com/feel-the-news/ftn-crawler/commits/master) [![coverage report](https://gitlab.com/feel-the-news/ftn-crawler/badges/master/coverage.svg)](https://gitlab.com/feel-the-news/ftn-crawler/commits/master)


Python RSS feed parser that collects news articles and dumps them to Google BigQuery.


## Setup and run crawler locally

Save the credentials to Google Cloud platform locally as a JSON file to `key.json`.. To generate the key file, follow 
the instructions [here](https://cloud.google.com/docs/authentication/production#obtaining_and_providing_service_account_credentials_manually).

```bash
$ git clone git@gitlab.com:feel-the-news/ftn-crawler.git
$ cd ftn-crawler
$ sudo docker build -t crawler -f  container/Dockerfile . # Build image
$ sudo docker run crawler python3 -m unittest # Run unit tests
$ sudo docker run crawler pycodestyle --show-source --show-pep8 src # Pep8 validation
$ sudo docker run crawler python3 src/rss_parser.py --gc_path key.json # Send RSS data to BigQuery table
```

